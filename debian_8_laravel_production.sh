#!/bin/bash

# How to use: First declare DEPLOY* variables. Afterwards run the wget. You should be root or use sudo.
# export DEPLOYUSER="username"; export DEPLOYFOLDER="/somewhere/laravel-projetcs/"; export DEPLOYDOMAIN="tester.com";
# wget -qO- https://raw.githubusercontent.com/mieserfettsack/server_install_scripts/master/debian_8_laravel_production.sh | bash -;

if [ -z ${DEPLOYUSER+x} ]; then
    echo "DEPLOYUSER is empty" ; exit 1;
fi

if [ -z ${DEPLOYDOMAIN+x} ]; then
    echo "DEPLOYDOMAIN is empty" ; exit 1;
fi

if [ -z ${DEPLOYFOLDER+x} ]; then
    echo "DEPLOYFOLDER is empty" ; exit 1;
fi

if [ -z ${MYSQLPASSWORD+x} ]; then
    echo "MYSQLPASSWORD is empty" ; exit 1;
fi

if [ -z ${DEPLOYMYSQLPASSWORD+x} ]; then
    echo "DEPLOYMYSQLPASSWORD is empty" ; exit 1;
fi

if [ -z ${DEPLOYHTUSER+x} ]; then
    echo "DEPLOYHTUSER is empty" ; exit 1;
fi

if [ -z ${DEPLOYHTPASSWORD+x} ]; then
    echo "DEPLOYHTPASSWORD is empty" ; exit 1;
fi

# Apt um Quellen erweitern
export DEBIAN_FRONTEND=noninteractive;
mkdir -p ${DEPLOYFOLDER};
mkdir -p /opt/deploy_laravel/;
cd ~;
apt-get -qq install apt-transport-https;
apt-get -qq remove --Purge mysql* php* apache*;
apt-get -qq update;
apt-get -qq upgrade;
apt-get -qq autoremove;
wget -qO- https://www.dotdeb.org/dotdeb.gpg | apt-key add -;
echo "deb http://packages.dotdeb.org jessie all" | tee /etc/apt/sources.list.d/dotdeb.list;
echo "deb-src http://packages.dotdeb.org jessie all" | tee -a /etc/apt/sources.list.d/dotdeb.list;
wget -qO- https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -;
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list;
apt-get -qq update;

# Erst php installieren, da composer es braucht.
apt-get -qq install php7.0-cli php7.0-curl;
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');";
php composer-setup.php;
php -r "unlink('composer-setup.php');";
mv composer.phar /usr/local/bin/composer;

debconf-set-selections <<< "maria-db-10.0 mysql-server/root_password password ${MYSQLPASSWORD}"
debconf-set-selections <<< "maria-db-10.0 mysql-server/root_password_again password ${MYSQLPASSWORD}"

# npm in der 4er Version installieren
wget -qO- https://deb.nodesource.com/setup_4.x | bash -;
apt-get -qq install sudo vim htop php7.0-dev php7.0-fpm php7.0-gd php7.0-mysql php7.0-mcrypt php7.0-opcache nginx mariadb-server git php7.0-mbstring php7.0-xml unzip yarn nodejs apache2-utils;
npm install -gy gulp-cli;

# Benutzer anlegen
useradd -m -s /bin/bash ${DEPLOYUSER};
mkdir -p /home/${DEPLOYUSER}/.ssh/;
chown -R ${DEPLOYUSER}:${DEPLOYUSER} /home/${DEPLOYUSER}/.ssh/;
su -s /bin/bash -c 'ssh-keygen -t rsa -N "" -f /home/'${DEPLOYUSER}'/.ssh/id_rsa' ${DEPLOYUSER};
cat > /home/${DEPLOYUSER}/.ssh/config << EOL
Host github.com
    StrictHostKeyChecking no

Host bitbucket.org
    StrictHostKeyChecking no
EOL
chown ${DEPLOYUSER}:${DEPLOYUSER} /home/${DEPLOYUSER}/.ssh/config
chmod 600 /home/${DEPLOYUSER}/.ssh/config
touch /home/${DEPLOYUSER}/.ssh/authorized_keys
chown ${DEPLOYUSER}:${DEPLOYUSER} /home/${DEPLOYUSER}/.ssh/authorized_keys
chmod 600 /home/${DEPLOYUSER}/.ssh/authorized_keys

# nginx config anlegen
mkdir -p /etc/nginx/configs/;

cat > /root/.my.cnf << EOL
[client]
user=root
password="${MYSQLPASSWORD}"

[mysql]
user=root
password="${MYSQLPASSWORD}"

[mysqldump]
user=root
password="${MYSQLPASSWORD}"

[mysqldiff]
user=root
password="${MYSQLPASSWORD}"
EOL

cat > /home/${DEPLOYUSER}/.my.cnf << EOL
[client]
user=${DEPLOYUSER}
password="${DEPLOYMYSQLPASSWORD}"

[mysql]
user=${DEPLOYUSER}
password="${DEPLOYMYSQLPASSWORD}"

[mysqldump]
user=${DEPLOYUSER}
password="${DEPLOYMYSQLPASSWORD}"

[mysqldiff]
user=${DEPLOYUSER}
password="${DEPLOYMYSQLPASSWORD}"
EOL
chown ${DEPLOYUSER}:${DEPLOYUSER} /home/${DEPLOYUSER}/.my.cnf
chmod 600 /home/${DEPLOYUSER}/.my.cnf

# create mysql user
mysql -e "CREATE USER '${DEPLOYUSER}'@'localhost' IDENTIFIED BY '${DEPLOYMYSQLPASSWORD}';";
mysql -e "GRANT ALL PRIVILEGES ON *.* TO '${DEPLOYUSER}'@'localhost' WITH GRANT OPTION;";
mysql -e "FLUSH PRIVILEGES;";

cat > /etc/nginx/configs/laravel_default_location << EOL
listen 80;
index index.html index.htm index.php;
charset utf-8;
location = /favicon.ico { access_log off; log_not_found off; }
location = /robots.txt  { access_log off; log_not_found off; }
access_log off;
sendfile off;
client_max_body_size 100m;
EOL

cat > /etc/nginx/configs/laravel_default_fastcgi << EOL
fastcgi_split_path_info ^(.+\.php)(/.+)\$;
fastcgi_index index.php;
include fastcgi_params;
fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
fastcgi_intercept_errors off;
fastcgi_buffer_size 16k;
fastcgi_buffers 4 16k;
fastcgi_connect_timeout 300;
fastcgi_send_timeout 300;
fastcgi_read_timeout 300;
EOL

cat > /opt/deploy_laravel/laravel_nginx_site_default_config << EOL
server {
    include /etc/nginx/configs/laravel_default_location;

    server_name REPLACEMESTRING.${DEPLOYDOMAIN};
    root "${DEPLOYFOLDER}/REPLACEMESTRING/htdocs/public";

    location / {
        auth_basic "Nope!";
        auth_basic_user_file ${DEPLOYFOLDER}/.htpasswd;
        try_files \$uri \$uri/ /index.php?\$query_string;
    }

    error_log  /var/log/nginx/REPLACEMESTRING.error.log error;

    location ~ \.php\$ {
        fastcgi_pass unix:/var/run/php/REPLACEMESTRING.sock;
        include /etc/nginx/configs/laravel_default_fastcgi;
    }

    location ~ /\.ht {
        deny all;
    }
}
EOL

htpasswd -b -c ${DEPLOYFOLDER}/.htpasswd ${DEPLOYHTUSER} ${DEPLOYHTPASSWORD}

cat > /opt/deploy_laravel/laravel_php_pool_default_config << EOL
[REPLACEMESTRING]
user = REPLACEMESTRING
group = REPLACEMESTRING
listen = /var/run/php/REPLACEMESTRING.sock
listen.owner = www-data
listen.group = www-data
pm = dynamic
pm.max_children = 5
pm.start_servers = 2
pm.min_spare_servers = 1
pm.max_spare_servers = 3
pm.max_requests = 200
chdir = /
EOL

cat > /etc/sudoers.d/${DEPLOYUSER} << EOL
${DEPLOYUSER} ALL = (root) NOPASSWD: /opt/deploy_laravel/create_user.sh
${DEPLOYUSER} ALL = (root) NOPASSWD: /opt/deploy_laravel/add_deploy_user_to_projects_group.sh
${DEPLOYUSER} ALL = (root) NOPASSWD: /opt/deploy_laravel/remove_project_from_deploy_server.sh
${DEPLOYUSER} ALL = (root) NOPASSWD: /opt/deploy_laravel/create_new_prjoect.sh
${DEPLOYUSER} ALL = (root) NOPASSWD: /opt/deploy_laravel/create_permissions.sh
EOL

cat > /opt/deploy_laravel/create_user.sh << EOL
#!/bin/bash
if [ \$# -ge 1 ]; then
    PROJECTNAME=\$1
    groupadd \${PROJECTNAME}
    useradd -g \${PROJECTNAME} \${PROJECTNAME}
else
    echo "PROJECTNAME is empty" ; exit 1;
fi
EOL

cat > /opt/deploy_laravel/add_deploy_user_to_projects_group.sh << EOL
#!/bin/bash
if [ \$# -ge 1 ]; then
    PROJECTNAME=\$1
    usermod -a -G \${PROJECTNAME} ${DEPLOYUSER}
else
    echo "add_deploy_user_to_projects_group.sh: PROJECTNAME is empty" ; exit 1;
fi
EOL

cat > /opt/deploy_laravel/remove_project_from_deploy_server.sh << EOL
#!/bin/bash
if [ \$# -ge 1 ]; then
    PROJECTNAME=\$1
    rm -Rf ${DEPLOYFOLDER}/\${PROJECTNAME}
    echo "Deleting user: \${PROJECTNAME}"
    userdel -fr \${PROJECTNAME} > /dev/null 2>&1
    groupdel \${PROJECTNAME} > /dev/null 2>&1
    echo "Deleting nginx and php config: \${PROJECTNAME}"
    rm -f /etc/nginx/sites-enabled/\${PROJECTNAME}.conf
    rm -f /etc/php/7.0/fpm/pool.d/\${PROJECTNAME}.conf
    echo "Deleting mysql user and database: \${PROJECTNAME}"
    mysql -e "DROP DATABASE '\${PROJECTNAME}';"
    mysql -e "DROP USER '\${PROJECTNAME}'@'localhost';"
    echo "Restarting services"
    /etc/init.d/nginx restart > /dev/null 2>&1
    /etc/init.d/php7.0-fpm restart > /dev/null 2>&1
else
    echo "remove_project_from_deploy_server.sh: PROJECTNAME is empty" ; exit 1;
fi
EOL

cat > /opt/deploy_laravel/create_new_prjoect.sh << EOL
#!/bin/bash
if [ \$# -ge 1 ]; then
    PROJECTNAME=\$1
    echo "Creating folder: \${PROJECTNAME}"
    mkdir -p ${DEPLOYFOLDER}/\${PROJECTNAME}
    echo "Creating nginx and fpm settings: \${PROJECTNAME}"
    cat /opt/deploy_laravel/laravel_nginx_site_default_config | sed -e 's/REPLACEMESTRING/'\${PROJECTNAME}'/g' > /etc/nginx/sites-enabled/\${PROJECTNAME}.conf
    cat /opt/deploy_laravel/laravel_php_pool_default_config | sed -e 's/REPLACEMESTRING/'\${PROJECTNAME}'/g' > /etc/php/7.0/fpm/pool.d/\${PROJECTNAME}.conf
    echo "Restarting services"
    /etc/init.d/nginx restart > /dev/null 2>&1
    /etc/init.d/php7.0-fpm restart > /dev/null 2>&1
else
    echo "remove_project_from_deploy_server.sh: PROJECTNAME is empty" ; exit 1;
fi
EOL

cat > /opt/deploy_laravel/create_permissions.sh << EOL
#!/bin/bash
if [ \$# -ge 1 ]; then
    PROJECTNAME=\$1
    chown -R \${PROJECTNAME}:\${PROJECTNAME} ${DEPLOYFOLDER}/\${PROJECTNAME}
    chmod -R 775 ${DEPLOYFOLDER}/\${PROJECTNAME}
    chmod -R 777 ${DEPLOYFOLDER}/\${PROJECTNAME}/htdocs/storage/
else
    echo "create_permissions.sh: PROJECTNAME is empty" ; exit 1;
fi
EOL

cat > /etc/environment << EOL
DEPLOYDOMAIN=${DEPLOYDOMAIN}
EOL

chmod +x /opt/deploy_laravel/create_user.sh
chmod +x /opt/deploy_laravel/add_deploy_user_to_projects_group.sh
chmod +x /opt/deploy_laravel/remove_project_from_deploy_server.sh
chmod +x /opt/deploy_laravel/create_new_prjoect.sh
chmod +x /opt/deploy_laravel/create_permissions.sh

# Öffentlichen ssh Key für den User ausgeben
cat "/home/$DEPLOYUSER/.ssh/id_rsa.pub";
