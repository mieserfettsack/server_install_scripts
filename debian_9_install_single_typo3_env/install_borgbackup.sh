#!/bin/bash

# Will add a borgbackup cronjob to this TYPO3-site.
# wget --no-check-certificate -qO- https://raw.githubusercontent.com/mieserfettsack/server_install_scripts/master/debian_9_install_single_typo3_env/install_borgbackup.sh | bash -;

apt-get -y -qq update > /dev/null 2>&1;
apt-get -y -qq install borgbackup > /dev/null 2>&1;

if [ -z ${BACKUPSERVERHOST+x} ]; then
	echo "Set BACKUPSERVERHOST:";
	read BACKUPSERVERHOST;
fi

if [ -z ${BACKUPSERVERUSER+x} ]; then
	echo "Set BACKUPSERVERUSER:";
	read BACKUPSERVERUSER;
fi

rm -f /root/.ssh/id_rsa > /dev/null 2>&1;
ssh-keygen -t rsa -N "" -f /root/.ssh/id_rsa > /dev/null 2>&1;
cat /root/.ssh/id_rsa.pub;

echo "Copy Public SSH key to ${BACKUPSERVERUSER}@${BACKUPSERVERHOST}:~/.ssh/authorized_keys! Done?";
read REPLY;
if [[ $REPLY =~ ^[Yy]$ ]]; then
	borg init --encryption=none ${BACKUPSERVERUSER}@${BACKUPSERVERHOST}:~/$(hostname);
	(crontab -l 2>/dev/null; echo "25 * * * * /usr/bin/nice --20 /usr/bin/borg create -p -C zlib,6 ${BACKUPSERVERUSER}@${BACKUPSERVERHOST}:~/$(hostname)::$(hostname)-\$(date +\%Y-\%m-\%d-\%H-\%M) --exclude-caches --exclude-from /root/.borg.exclude.list \$(cat /root/.borg.include.list | tr \"\n\" \" \") > /dev/null 2>&1") | crontab -;
	(crontab -l 2>/dev/null; echo "55 1 * * * /usr/bin/nice --20 /usr/bin/borg prune --keep-hourly=12 --keep-daily=7 --keep-weekly=52 --keep-yearly=5 ${BACKUPSERVERUSER}@${BACKUPSERVERHOST}:~/$(hostname) > /dev/null 2>&1") | crontab -;
fi