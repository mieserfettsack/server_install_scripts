#!/bin/bash

# How to use: First declare variables. Afterwards run the wget. You should be root or use sudo.
# export TYPO3DOMAIN="tester.com" TYPO3VERSION="8" MYSQLROOTPASSWORD="MYSQLROOTPASSWORD" DATABASEPASSWORD="DATABASEPASSWORD";
# wget --no-check-certificate -qO- https://raw.githubusercontent.com/mieserfettsack/server_install_scripts/master/debian_9_install_single_typo3_env/install.sh | bash -;

if [ -z ${TYPO3DOMAIN+x} ]; then
    TYPO3DOMAIN="typo3.test";
    echo "TYPO3DOMAIN is set to ${TYPO3DOMAIN}" ;
fi

if [ -z ${TYPO3VERSION+x} ]; then
    TYPO3VERSION="8";
    echo "TYPO3VERSION is set to ${TYPO3VERSION}" ;
fi

if [ -z ${MYSQLROOTPASSWORD+x} ]; then
    MYSQLROOTPASSWORD="MYSQLROOTPASSWORD";
    echo "MYSQLROOTPASSWORD is set to ${MYSQLROOTPASSWORD}";
fi

if [ -z ${DATABASENAME+x} ]; then
    DATABASENAME=${TYPO3DOMAIN//[.-]/_};
    echo "DATABASENAME is set to ${DATABASENAME}";
fi

if [ -z ${DATABASEUSER+x} ]; then
    DATABASEUSER=${TYPO3DOMAIN//[.-]/_};
    echo "DATABASEUSER is set to ${DATABASEUSER}";
fi

if [ -z ${DATABASEPASSWORD+x} ]; then
    DATABASEPASSWORD="DATABASEPASSWORD";
    echo "DATABASEPASSWORD is set to ${DATABASEPASSWORD}";
fi

if [ -z ${HTUSER+x} ]; then
    HTUSER="lets";
    echo "HTUSER is set to ${HTUSER}";
fi

if [ -z ${HTPASSWORD+x} ]; then
    HTPASSWORD="go";
    echo "HTPASSWORD is set to ${HTPASSWORD}";
fi

#set variables/conf needed during installation
export DEBIAN_FRONTEND=noninteractive;
debconf-set-selections <<< "maria-db-10.0 mysql-server/root_password password ${MYSQLROOTPASSWORD}";
debconf-set-selections <<< "maria-db-10.0 mysql-server/root_password_again password ${MYSQLROOTPASSWORD}";

# install command needed during installation
apt-get -y -qq update > /dev/null 2>&1;
apt-get -y -qq install apt-transport-https ca-certificates wget gnupg2 software-properties-common > /dev/null 2>&1;

# Prepare needed folders
mkdir -p /srv/httpd/${TYPO3DOMAIN}/htdocs;
mkdir -p /srv/httpd/${TYPO3DOMAIN}/vendor/typo3;
mkdir -p /srv/typo3;
mkdir -p /opt/typo3_tools/;

# install given TYPO3 version
cd /srv/typo3;
wget -q --no-check-certificate --content-disposition get.typo3.org/${TYPO3VERSION};
INSTALLTYPO3VERSION=`ls -t | head -n1`;
tar xfz ${INSTALLTYPO3VERSION};
rm ${INSTALLTYPO3VERSION};
ln -snf ${INSTALLTYPO3VERSION::-7} typo3-${TYPO3VERSION};

# create typo3 symlinks
cd /srv/httpd/${TYPO3DOMAIN}/vendor/typo3;
ln -snf /srv/typo3/typo3-${TYPO3VERSION} .;
cd /srv/httpd/${TYPO3DOMAIN}/htdocs;
ln -snf ../vendor/typo3/typo3-${TYPO3VERSION} typo3_src;
ln -snf typo3_src/typo3/ .;
ln -snf typo3_src/index.php .;

cd /root/;
# add docker-ce repository
wget --no-check-certificate -qO- https://download.docker.com/linux/ubuntu/gpg | apt-key add - > /dev/null 2>&1;
add-apt-repository "deb [arch=$(dpkg --print-architecture)] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") $(lsb_release -cs) stable"  > /dev/null 2>&1;

apt-get -y -qq remove --Purge mysql* php* apache* nginx* > /dev/null 2>&1;
apt-get -y -qq autoremove > /dev/null 2>&1;
apt-get -y -qq update > /dev/null 2>&1;
apt-get -y -qq upgrade > /dev/null 2>&1;
apt-get -y -qq install docker-ce vim imagemagick graphicsmagick htop php7.0-apcu php7.0-dev php7.0-bz2 php7.0-zip php7.0-fpm php7.0-gd php7.0-mysql php7.0-mcrypt php7.0-opcache nginx mariadb-server git php7.0-mbstring php7.0-xml unzip apache2-utils > /dev/null 2>&1;

cat > /root/.my.cnf << EOL
[client]
user=root
password="${MYSQLROOTPASSWORD}"

[mysql]
user=root
password="${MYSQLROOTPASSWORD}"

[mysqldump]
user=root
password="${MYSQLROOTPASSWORD}"

[mysqldiff]
user=root
password="${MYSQLROOTPASSWORD}"
EOL

# create mysql user
mysql -e "CREATE DATABASE ${DATABASENAME};";
mysql -e "ALTER DATABASE ${DATABASENAME} CHARACTER SET utf8 COLLATE utf8_general_ci;";
mysql -e "GRANT ALL PRIVILEGES ON ${DATABASENAME}.* TO ${DATABASEUSER}@localhost IDENTIFIED BY '${DATABASEPASSWORD}';";
mysql -e "GRANT ALL PRIVILEGES ON ${DATABASENAME}.* TO ${DATABASEUSER}@localhost.localdomain IDENTIFIED BY '${DATABASEPASSWORD}';";
mysql -e "FLUSH PRIVILEGES;";

# add additional php settings to avoid warnings in typo3 install tool
cat > /etc/php/7.0/fpm/conf.d/30-typo3.ini << EOL
max_execution_time=240
max_input_vars=1500
upload_max_filesize = 200M
post_max_size = 200M
EOL

# add additional php settings so apcu can be used
cat > /etc/php/7.0/cli/conf.d/30-typo3.ini << EOL
apc.enable_cli=1
EOL

cat > /etc/nginx/sites-available/${TYPO3DOMAIN} << EOL
server {
    server_name ${TYPO3DOMAIN};
    root "/srv/httpd/${TYPO3DOMAIN}/htdocs/";
    listen 80;
    index index.html index.htm index.php;
    charset utf-8;
    access_log off;
    sendfile off;
    client_max_body_size 100m;
    error_log  /var/log/nginx/${TYPO3DOMAIN}.error.log error;

    location ~ ^/(favicon.ico|robots.txt)/ {
        access_log off;
        log_not_found off;
    }

    location / {
        auth_basic "Nope!";
        auth_basic_user_file /srv/httpd/${TYPO3DOMAIN}/.htpasswd;
        try_files $uri $uri/ @sfc;
    }

    location =/ {
        recursive_error_pages on;
        error_page 405 = @sfc;
        return 405;
    }

    location @t3frontend {
        try_files $uri /index.php$is_args$args;
    }

    location @sfc {
        error_page 405 = @t3frontend;

        if ($args != '') {
            return 405;
        }

        if ($cookie_nc_staticfilecache = 'fe_typo_user_logged_in') {
            return 405;
        }
        if ($cookie_be_typo_user != '') {
            return 405;
        }

        if ($request_method !~ ^(GET|HEAD)$ ) {
            return 405;
        }

        charset utf8;

        try_files
            /typo3temp/tx_ncstaticfilecache/${scheme}/${host}${uri}/index.html
            /typo3temp/tx_ncstaticfilecache/${scheme}/${host}${uri}
            =405
        ;
    }

    location /typo3temp/tx_ncstaticfilecache {
        deny all;
    }

    location ~ \.php\$ {
        fastcgi_pass unix:/var/run/php/${TYPO3DOMAIN}.sock;
        fastcgi_split_path_info ^(.+\.php)(/.+)\$;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        fastcgi_intercept_errors off;
        fastcgi_buffer_size 16k;
        fastcgi_buffers 4 16k;
        fastcgi_connect_timeout 300;
        fastcgi_send_timeout 300;
        fastcgi_read_timeout 300;
    }

    location ~ /\.ht {
        deny all;
    }
}
EOL

ln -snf  /etc/nginx/sites-available/${TYPO3DOMAIN} /etc/nginx/sites-enabled/${TYPO3DOMAIN}

groupadd ${TYPO3DOMAIN};
useradd -g ${TYPO3DOMAIN} ${TYPO3DOMAIN};

htpasswd -b -c /srv/httpd/${TYPO3DOMAIN}/.htpasswd ${HTUSER} ${HTPASSWORD} > /dev/null 2>&1;

cat > /etc/php/7.0/fpm/pool.d/${TYPO3DOMAIN}.conf << EOL
[${TYPO3DOMAIN}]
user = ${TYPO3DOMAIN}
group = ${TYPO3DOMAIN}
listen = /var/run/php/${TYPO3DOMAIN}.sock
listen.owner = www-data
listen.group = www-data
pm = dynamic
pm.max_children = 5
pm.start_servers = 2
pm.min_spare_servers = 1
pm.max_spare_servers = 3
pm.max_requests = 200
chdir = /
EOL

cat > /opt/typo3_tools/test_docker.sh << EOL
#!/bin/bash
if [[ \$(docker ps --format '{{.Image}}' | head -c1 | wc -c) -ne 1 ]]; then /usr/bin/docker run --log-driver=none -d -p 127.0.0.1:8983:8983 -v "/root/.solrdata:/opt/solr/server/solr/data/" typo3-solr; fi
EOL

cat > /root/.borg.exclude.list << EOL
*/typo3temp/*
*/_processed_/*
/var/cache/*
EOL

cat > /root/.borg.include.list << EOL
/srv
/root
/var/log
/var/lib/docker
/var/lib/mysql
/var/spool
/etc/nginx
/etc/mysql
/etc/php
/opt
EOL

chown -R ${TYPO3DOMAIN}:${TYPO3DOMAIN} /srv/httpd/${TYPO3DOMAIN}/htdocs
chmod -R 775 /srv/httpd/${TYPO3DOMAIN}/htdocs

# install apache solr as docker

mkdir -p /root/.solrdata;
chmod g+w /root/.solrdata;
chown :8983 /root/.solrdata;
cd /root/;
git clone https://github.com/TYPO3-Solr/ext-solr.git > /dev/null 2>&1;
cd /root/ext-solr/;
docker build -q -t typo3-solr . > /dev/null 2>&1;

(crontab -l 2>/dev/null; echo "@reboot /usr/bin/docker run --log-driver=none -d -p 127.0.0.1:8983:8983 -v \"/root/.solrdata:/opt/solr/server/solr/data/\" typo3-solr") | crontab -;
(crontab -l 2>/dev/null; echo "*/2 * * * * /bin/bash /opt/typo3_tools/test_docker.sh > /dev/null 2>&1") | crontab -;

/etc/init.d/nginx restart > /dev/null 2>&1;
/etc/init.d/php7.0-fpm restart > /dev/null 2>&1;

/usr/bin/docker run --log-driver=none -d -p 127.0.0.1:8983:8983 -v "/root/.solrdata:/opt/solr/server/solr/data/" typo3-solr > /dev/null 2>&1;
touch /srv/httpd/${TYPO3DOMAIN}/htdocs/FIRST_INSTALL;

echo "Done!";
